CPMAddPackage(
  NAME nlohmann_json
  URL https://github.com/nlohmann/json/archive/refs/tags/v3.10.5.zip
  VERSION 3.10.5
)

CPMAddPackage("gh:gabime/spdlog@1.8.2")
CPMAddPackage("gh:yhirose/cpp-httplib@0.9.9")
CPMAddPackage("gh:neargye/semver@0.3.0")

CPMAddPackage(
    NAME libzmq
    VERSION 4.3.4
    GITHUB_REPOSITORY zeromq/libzmq
    OPTIONS
      "ZMQ_BUILD_TESTS Off"
)

CPMAddPackage(
    NAME cppzmq
    VERSION 4.8.1
    GITHUB_REPOSITORY zeromq/cppzmq
    OPTIONS
      "CPPZMQ_BUILD_TESTS Off"
)

CPMAddPackage(
  NAME sdk
  VERSION 3.02
  GIT_REPOSITORY https://gitlab.com/blrevive/tools/sdk
  GIT_TAG 3.02
)