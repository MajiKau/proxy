#pragma once
#include <memory>
#include <string>
#include <functional>
#include <Proxy/Utils.h>
#include <nlohmann/json.hpp>
#include <Proxy/Config.h>
#include <Proxy/Utils.h>

#include <httplib.h>

using json = nlohmann::json;

namespace BLRevive::Network
{
	enum class RequestType { GET, POST };

	/**
	* Server manager
	*/
	class Server : public Utils::ProcessSingleton<Server>
	{
	public:
		using Handler = httplib::Server::Handler;

		/**
		 * Add connection handler to server.
		 * Only works in InitializeModule function!
		 * 
		 * @param  type 		type of request (GET|POST)
		 * @param  pattern 		route to listen
		 * @param  handler 		connection handler
		 */
		inline void AddConnectionHandler(RequestType type, std::string pattern, Handler handler)
		{
			if (bStarted)
				throw new Errors::failure("Can't add connection handler when server is started!");

			switch (type) {
			case RequestType::GET:
				_Server.Get(pattern, handler);
				break;
			case RequestType::POST:
				_Server.Post(pattern, handler);
				break;
			}
		};

#if IS_PROXY_PROJECT
		/**
		 * Start server in new thread
		 */
		void Start()
		{
			CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)StartListen, this, NULL, NULL);
			bStarted = true;
		}

		/**
		 * Start listening for connections
		 */
		static void StartListen() { _self->Listen(); }

		/**
		 * Listen for connections
		 */
		void Listen() 
		{ 
			try {
				_Server.listen(_Host.c_str(), _Port);
			}
			catch (int e) {
				throw new Errors::failure("Couldn't start server on " + _Host + ":" + std::to_string(_Port));
			}
		}

		Server(std::string Host, int Port) : _Host(Host), _Port(Port) {}
#endif
	private:
		bool bStarted = false;
		httplib::Server _Server;
		std::string _Host;
		int _Port;
	};
}