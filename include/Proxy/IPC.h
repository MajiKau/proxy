#pragma once

#include <string>
#include <nlohmann/json.hpp>

namespace BLRevive::IPC
{
	struct Result
	{

	};

	struct Request
	{
	};

	class IPCInterface
	{
	protected:
		bool Send(std::string address, std::string message, std::function<void(std::string, std::string)> callback = nullptr);
		bool Handle(std::string address, std::string message);
	};

	class Client
	{
	public:

		bool Connect(std::string server);

		Result SendRequest(Request &r);

		Result HandleRequest(Request& r);
	};

	class Server
	{
	public:
		bool HandleNewConnection(std::string clientAddress);

		Result SendRequest(std::string client, Request& r);
		Result HandleRequest(std::string client, Request& r);
	};
};