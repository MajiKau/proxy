#pragma once
#include <vector>
#include <map>
#include <functional>
#include <algorithm>
#include <iterator>
#include <Proxy/Utils.h>
#include <queue>

// forward declarations
class UObject;
class UFunction;

/**
 * Get the memory address of a std::function (for comparison)
 * 
 * @tparam T		func return param
 * @tparam U		function params
 * @param  f		std::function
 * @return size_t	memory address  
 */
template<typename T, typename... U>
size_t GetStdFuncAddress(std::function<T(U...)> f) {
	typedef T(fnType)(U...);
	fnType** fnPointer = f.template target<fnType*>();
	return (size_t)*fnPointer;
}

namespace BLRevive::Events
{
	/**
	 * Event Identifier
	 */
	struct ID
	{
	public:
		/**
		 * Create EventID from strings
		 * 
		 * @param  sObj		object name
		 * @param  sFunc	function name    
		 */
		ID(std::string sObj, std::string sFunc)
			: sObject(sObj), sFunction(sFunc) {}

		// object and function name (only valid for IDType::Name)
		std::string sObject;
		std::string sFunction;

		/**
		 * Compare EventIDs
		 * 
		 * @param  id1		first id
		 * @param  id2		second id
		 * @return bool		is same event      
		 */
		friend bool operator==(const ID& id1, const ID& id2)
		{
			return id1.sObject == id2.sObject && id1.sFunction == id2.sFunction;
		}
	};

	/**
	 * Information about a particalur event
	 */
	struct Info
	{
		// caller object
		UObject* Object;
		// called function
		UFunction* Function;
		// call parameters
		void* Params;

		Info(UObject* pObject, UFunction* pFunction, void* pParams)
			: Object(pObject), Function(pFunction), Params(pParams) {};
	};

	/**
	 * Event Handler
	 */
	struct Handler
	{
		// event id which triggers this handler
		ID EventID;
		// callback to execute when triggered
		std::function<void(Info)> Callback;
		// run the handler immediatly (blocking native ProcessEvent)
		// (use if access to objects is needed which are destroyed after the event)
		bool Immediate;
		// run the handler only one time (and remove it afterwards)
		bool SingleRun;

		/**
		 * Create new event handler
		 * 
		 * @param  id			event identifier
		 * @param  callback		function to execute
		 * @param  immediate	run immedietly (blocking ProcessEvent)
		 * @param  singleRun	run only one time
		 */
		Handler(ID id, std::function<void(Info)> callback, bool immediate = false, bool singleRun = false)
			: EventID(id), Callback(callback), Immediate(immediate), SingleRun(singleRun) {};
		~Handler() {}

		/**
		 * compare event handler
		 * 
		 * @param  h1		first handler   
		 * @param  h2		second handler  
		 * @return bool		same handler 
		 */
		inline friend bool operator==(const Handler& h1, const Handler& h2) {
			return GetStdFuncAddress(h1.Callback) == GetStdFuncAddress(h2.Callback) &&
				h1.Immediate == h2.Immediate &&
				h1.SingleRun == h2.SingleRun;
		};
	};

	/**
	 * Event Manager
	 */
	class Manager : public Utils::ProcessSingleton<Manager>
	{
	public:
		class failure : public std::exception { failure(const char* msg) : std::exception(msg) {}; };

		// we need different declarations of the manager in Proxy and the Modules
		// because some methods need access to UObject & UFunction members which are not provided in modules.
#if IS_PROXY_PROJECT
		static inline std::shared_ptr<Manager> Create()
		{
			if (_self == NULL) {
				_self = std::make_shared<Manager>();
				_self->Initialize();
			}

			return _self;
		}

		/**
		 * 	Check and update events from within ProcessEvent.
		 *  Executes immediate handlers and pushes lazy handlers on queue.
		 * 
		 * @param  pObj		caller object                
		 * @param  pFunc	called function  
		 * @param  pParams	call parameters
		 */
		void Tick(UObject* pObj, UFunction* pFunc, void* pParams);

		/**
		 * Process all lazy events
		 */
		void ProcessLazyEvents();

		/**
		 * Initialize event manager \n
		 * 	(hooks ProcessEvent to retrieve event infos)
		 * 
		 * @return bool	initialization succeed
		 */
		bool Initialize();
#endif
		/**
		 * Register a new event handler
		 * 
		 * @param  handler	handler to register
		 */
		inline void RegisterHandler(Handler handler) {  _Handlers.push_back(handler); };

		/**
		 * Remove an event handler
		 * 
		 * @param  handler	handler to remove 
		 */
		inline void RemoveHandler(Handler handler) { _Handlers.erase(std::find(_Handlers.begin(), _Handlers.end(), handler)); };

	protected:
		// registered handlers
		std::list<Handler> _Handlers;
		// queued events
		std::queue<Info> _Events;

		HANDLE handlerMutex;
	};
}