#pragma once

namespace BLRevive::Errors
{
	enum class ExitReason
	{
		UNKNOWN_ERROR = 40000,
		INIT_MAIN_FAILED,
		LOG_INIT_FAIL,
		CONFIG_INIT_FAIL,

	};

	class failure : public std::exception
	{
		using ReportLevel = spdlog::level::level_enum;
	public:
		failure(const char* msg, ReportLevel reportLevel = ReportLevel::err)
			: std::exception(msg), _reportLevel(reportLevel), _shouldExit(false), _exitReason(ExitReason::UNKNOWN_ERROR)
		{
			if (Logger::GetInstance() && Logger::GetInstance()->Log())
				Logger::GetInstance()->Log()->log(_reportLevel, msg);
			else
				spdlog::log(_reportLevel, msg);
		}

		failure(std::string msg, ReportLevel reportLevel = ReportLevel::err)
			: failure(msg.c_str(), reportLevel) {};

		failure(const char* msg, ExitReason reason)
			: std::exception(msg), _reportLevel(ReportLevel::critical), _exitReason(reason), _shouldExit(true) 
		{
			if (Logger::GetInstance() && Logger::GetInstance()->Log())
				Logger::GetInstance()->Log()->log(_reportLevel, msg);
			else
				spdlog::log(_reportLevel, msg);
		}

		bool shouldExit() { return true; }
		ExitReason exitReason() { return _exitReason; }
		int reportLevel() { return _reportLevel; }
	protected:
		ReportLevel _reportLevel;
		bool _shouldExit;
		ExitReason _exitReason;
	};
}