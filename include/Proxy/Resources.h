#pragma once
#include <string>
#include <windows.h>
#include <Proxy/Logger.h>
#include <nlohmann/json.hpp>

/**
* Get access to embbed resources defined inside {Project}.rc
*/
namespace BLRevive::Resources
{
	struct Parameters {
		std::size_t size = 0;
		void* ptr = nullptr;
		HRSRC res = 0;
	};

	template<typename T = Parameters>
	inline static T Get(int id, const std::string& cls = "TEXT", std::string moduleName = PROJECT_NAME)
	{
		T data;
		HMODULE hModule = GetModuleHandleA((moduleName + ".dll").c_str());
		HRSRC hResource = FindResourceA(hModule, MAKEINTRESOURCEA(id), cls.c_str());
		HGLOBAL hMemory = LoadResource(hModule, hResource);
		if (!hResource || !hMemory) {
			LError("Can't find resource {}", id);
			return data;
		}

		data.res = hResource;
		data.size = SizeofResource(hModule, hResource);
		data.ptr = LockResource(hMemory);
		return data;
	};

	template<>
	inline static std::string Get(int id, const std::string& cls, std::string moduleName)
	{
		auto data = Get<Parameters>(id);
		std::string dst(reinterpret_cast<char*>(data.ptr), data.size);
		FreeResource(data.res);
		return dst;
	};

	template<>
	inline static nlohmann::json Get(int id, const std::string& cls, std::string moduleName)
	{
		auto str = Get<std::string>(id);
		nlohmann::json j = nlohmann::json::parse(str);
		return j;
	};
}