#pragma once
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <Proxy/Utils.h>
#include <ctime>

#define LInfo(...) BLRevive::Logger::GetInstance()->Log()->info(__VA_ARGS__)
#define LWarn(...) BLRevive::Logger::GetInstance()->Log()->warn(__VA_ARGS__)
#define LError(...) BLRevive::Logger::GetInstance()->Log()->error(__VA_ARGS__)
#define LCritical(...) BLRevive::Logger::GetInstance()->Log()->critical(__VA_ARGS__)
#define LDebug(...) BLRevive::Logger::GetInstance()->Log()->debug(__VA_ARGS__)
#define LFlush BLRevive::Logger::GetInstance()->Log()->flush();

namespace BLRevive
{
	/**
	 * @brief	Logging interface
	 */
	class Logger : public Utils::ProcessSingleton<Logger>
	{
	public:
		inline std::shared_ptr<spdlog::logger> Log() { return _spdlogger; }

#if IS_PROXY_PROJECT
		inline static std::shared_ptr<Logger> Create(int level = 0) 
		{
			std::string logFileName(Utils::FS::BlreviveLogPath() + "\\" + LogFileName + "-" + std::to_string(std::time(NULL)) + ".log");
			auto spdlogger = spdlog::basic_logger_mt("BLR", logFileName.c_str());
			spdlogger->set_level((spdlog::level::level_enum)level);
			spdlogger->flush_on(spdlog::level::debug);
			_self = std::make_shared<Logger>(spdlogger);
			return _self;
		}

		Logger(std::shared_ptr<spdlog::logger> spdlogger) : _spdlogger(spdlogger) {};
#endif

	private:
		std::shared_ptr<spdlog::logger> _spdlogger;
		inline static std::string LogFileName = "BLRevive";
	};
}

