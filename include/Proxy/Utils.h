#pragma once
#include <windows.h>
#include <string>

namespace BLRevive::Utils
{
	/**
	 * 	A base class that ensures only a single instance of T is created
	 *	in the whole process.
	 *
	 * @tparam T class to make singleton
	 */
	template <typename T>
	class ProcessSingleton
	{
	public:
		/**
		 * Get current instance of singleton (may return null)
		 *
		 * @return pointer to instance
		 */
		inline static std::shared_ptr<T> GetInstance() { return _self; }

#if IS_PROXY_PROJECT
		/**
		 * Create instance of T as singleton
		 *
		 * @tparam ...Args types of constructor params
		 * @param args constructor params
		 * @return instance of T
		 */
		template <typename... Args>
		inline static std::shared_ptr<T> Create(Args... args)
		{
			if (_self == NULL)
				_self = std::make_shared<T>(args...);
			return _self;
		}
#else
		/**
		 * Update the instance pointer within modules.
		 *
		 * @param instance instance pointer
		 */
		inline static void Link(std::shared_ptr<T> instance) { _self = instance; }
#endif
	protected:
		inline static std::shared_ptr<T> _self = NULL;
	};

	/**
	 * Wether current instance is running server
	 *
	 * @return is server
	 */
	inline bool IsServer()
	{
		std::string cli(GetCommandLineA());
		return cli.find(" server ") != std::string::npos;
	}

	namespace URL::Param
	{
		/**
		 * Get cli url param as string
		 *
		 * @param key name of param
		 * @param def default value
		 * @return value or default if not found
		 */
		inline std::string String(std::string key, std::string def = "")
		{
			std::string cli(GetCommandLineA());

			int startPos = cli.find("?" + key);
			if (startPos == -1)
				startPos = cli.find("&" + key);
			if (startPos == -1)
				return def;

			int nextPos = cli.find("&", startPos + 1);
			if (nextPos == -1)
				nextPos = cli.size() - (startPos + 1);

			auto keyVal = cli.substr(startPos + 1, nextPos);
			int assign = keyVal.find("=");
			if (assign == -1)
				return def;

			return keyVal.substr(assign + 1);
		}

		/**
		 * Get url param as integer
		 *
		 * @param key name of param
		 * @param def default value
		 * @return value or default if not found
		 */
		inline int Int(std::string key, int def = -1)
		{
			std::string val = String(key);
			if (val.empty())
				return def;
			return std::stoi(val);
		}

		/**
		 * Get url param flag (if key exist)
		 *
		 * @param key name of param
		 * @return flag/key exists
		 */
		inline bool Bool(std::string key)
		{

			std::string cli(GetCommandLineA());

			int startPos = cli.find("?" + key);
			if (startPos == -1)
				startPos = cli.find("&" + key);
			if (startPos == -1)
				return false;
			return true;
		}
	};

	namespace FS
	{
		/**
		 * Get current work directory
		 *
		 * @return cwd
		 */
		static std::string CWD()
		{
			static std::string name;
			if (name.empty())
			{
				char buffer[MAX_PATH] = {0};
				GetModuleFileNameA(NULL, buffer, MAX_PATH);
				std::string::size_type pos = std::string(buffer).find_last_of("\\");
				name = std::string(buffer).substr(0, pos);
			}
			return name;
		}

		/**
		 * Get base path of blacklight (for eg "C:\Program Files (x86)\Steam\steamapps\common\blacklightretribution\")
		 *
		 * @return blr base path
		 */
		static std::string BlrBasePath()
		{
			static std::string _basePath;
			if (_basePath.empty())
				_basePath = CWD().substr(0, CWD().find("Binaries\\"));

			return _basePath;
		}

		/**
		 * Get config path
		 *
		 * @return config path
		 */
		static std::string BlreviveConfigPath() { return BlrBasePath() + "FoxGame\\Config\\BLRevive\\"; }

		/**
		 * Get log path
		 *
		 * @return log path
		 */
		static std::string BlreviveLogPath() { return BlrBasePath() + "FoxGame\\Logs\\"; }

		/**
		 * Get module path
		 *
		 * @return module path
		 */
		static std::string BlreviveModulePath() { return BlrBasePath() + "Binaries\\Win32\\Modules\\"; }
	};

	namespace Memory
	{
		/**
		 * Writes a JMP (0xE9) instruction at address.
		 *
		 * @param  pAddress		address to overwrite
		 * @param  dwJumpTo		jump destination
		 * @param  dwLen		length to override
		 * @return bool			jmp was written
		 */
		static bool WriteJMP(BYTE *pAddress, DWORD dwJumpTo, DWORD dwLen)
		{
			DWORD dwOldProtect, dwBkup, dwRelAddr;

			// set write access for destination
			if (!VirtualProtect(pAddress, dwLen, PAGE_EXECUTE_READWRITE, &dwOldProtect))
				throw std::exception("failed to change virtual protect");

			// overwrite the bytes with jmp instruction (0xE9)
			dwRelAddr = (DWORD)(dwJumpTo - (DWORD)pAddress) - 5;
			*pAddress = 0xE9;
			*((DWORD *)(pAddress + 0x1)) = dwRelAddr;
			for (DWORD x = 0x5; x < dwLen; x++)
				*(pAddress + x) = 0x90;

			VirtualProtect(pAddress, dwLen, dwOldProtect, &dwBkup);
			return true;
		}
	}
}