#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <windows.h>

#include <Proxy/Events.h>
#include <Proxy/Modules.h>
#include <Proxy/Logger.h>
#include <Proxy/Errors.h>
#include <Proxy/Network.h>
#include <Proxy/Config.h>
#include <Proxy/Resources.h>
#include <Proxy/resource.h>
#include <Proxy/Chat.h>

#include <zmq.h>


using namespace BLRevive;
using namespace BLRevive::Events;
using namespace BLRevive::Network;
using namespace BLRevive::Utils;

/**
 * Initializes the proxy and loads modules
 */
void InitializeProxy()
{
#if NDEBUG
    try {
#endif
        auto config = Config::Create(URL::Param::String("config", "default.json"));
        auto proxyConf = config->Get<ProxyConfig>("Proxy");

        // initialize server
        auto srv = Server::Create(proxyConf.Server.Host, proxyConf.Server.Port);

        // load modules 
        Module::Loader::Load();

        // start server
        if (Utils::IsServer())
            srv->Start();

        Chat::Init();

        // process lazy events in loop
        while (true) {
            Events::Manager::GetInstance()->ProcessLazyEvents();
            Sleep(200);
        }
#if NDEBUG
    }
    catch (Errors::failure e) {
        if (e.shouldExit()) {
            exit((int)e.exitReason());
        }
    }
    catch (std::exception e) {
        LCritical("Unhandled exception in InitializeProxy(): {}", e.what()); LFlush;
        exit((int)Errors::ExitReason::UNKNOWN_ERROR);
    }
    catch (int e) {
        LCritical("Unhandled exception in InitializeProxy(): {}", e); LFlush;
        exit((int)Errors::ExitReason::UNKNOWN_ERROR);
    }
#endif
}

/**
 * Wait for UE framework to boot and call InitializeProxy
 */
extern "C" _declspec(dllexport) void InitializeThread()
{
    try {
        // initialize logger
        Logger::Create();
        LDebug("initializing proxy startup thread");

        // initialize event manager
        auto eventMgr = Events::Manager::Create();

        // register event handler when WorldInfo is ready
        eventMgr->RegisterHandler({
            {"WorldInfo", "PreBeginPlay"},
            [](Events::Info eventInfo) {
                // create thread for initialization
                CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)InitializeProxy, NULL, NULL, NULL);
            }, true, true
        });
    }
    catch (Errors::failure e) {
        if (e.shouldExit())
            exit((int)e.exitReason());
    }
    catch (std::exception e) {
        LCritical("Unhandled exception in InitializeThread(): {}", e.what()); LFlush;
        exit((int)Errors::ExitReason::UNKNOWN_ERROR);
    }
    catch (int e) {
        LCritical("Unhandled exception in InitializeThread(): {}", e); LFlush;
        exit((int)Errors::ExitReason::UNKNOWN_ERROR);
    }
}



BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        InitializeThread();
        break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

