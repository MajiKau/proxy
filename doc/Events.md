Before we get into the actual API you should know what it actually does. Here's a little summary to get you started:

# Events API

Include `<Proxy/Events.h>` inside your module to get access to the event manager. 

## register handler

Add a new event handler using `Events::Manager` in order to listen to native UE3 events.

Handlers can either be executed *immedietly*, which means they are called inside and blocking the native `UObject::ProcessEvent`. Or they are executed lazy (non-blocking), which means the event information is pushed in a queue and processed in aonther thread later.

Also, you can define `Handler.SingleRun = true` to make the handler only run one time.

```c++
#include <Events.h>
using namespace BLRevive;

void AddPlayerTickEventHandler()
{
    // get pointer to event manager
    auto eventMgr = Events::Manager::GetInstance();

    // register an event handler
    eventMgr->RegisterHandler(
        new Events::ID("FoxPC", "Tick"),
        // event handler
        [](Events::Info e) {
            // cast object to the actual class
            auto PC = (AFoxPC*)e.Object;
            // get function params
            auto parms = (AFoxPC_eventTick_Parms*)e.Params;
        },
        true, // run the handler immedietly inside native ProcessEvent
        true // remove handler after first run
    )
}
```

### `Events::ID`

A unique identifier for events. Can be either pair of strings or pair of object and string:

```c++
// listen to event of Object and Function using strings
Events::ID("<ObjectName>", "<FunctionName>")
// can also use wildcard to listen to all
Events::ID("*", "*")
```

### `Events::Handler`

The actual event handler function with signature: `void (Events::Info)`.

### `Events::Info`

Informations about a specific event: 

- `UObject* Info.Object`: object instance of called function
- `UFunction* Info.Function`: called function
- `void* Info.Params`: params for function

# how it works
## summary of UE3 event handling

Unreal Engine uses `UObject::ProcessEvent` to process allmost all events raised (exceptions are native implementations). 

In order for a function to be called from [UnrealScript]() it is wrapped with `UObject::ProcessInternal` or `UObject::CallFunction` which are abstractions to the function.

All of these functions can be used to retrieve or manipulate data of the current game state.

## abusing `ProcessEvent` for custom event handling

It actually is pretty simple: if we hook into `UObject::ProcessEvent` we are able to retrieve the parameters of all events processed by Unreal Engine. The params we get are:

- `UObject* Object`: object instance of the called function
- `UFunction* Func`: function that has been called
- `void* Params`: paremeters of function

This is what the proxy actually does in essence: it hooks the native `UObject::ProcessEvent` using a mid-function hook, retrieves the parameters and provide them to the Event Manager.

And this is where you come in to play


